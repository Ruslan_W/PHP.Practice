<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 13.12.2017
 * Time: 10:21
 */
$days = [
    1 => "Понеділок",
    2 => "Вівторок",
    3 => "Середа",
    4 => "Четвер",
    5 => "Пятниця",
    6 => "Субота",
    7 => "Неділя",
];
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
</head>
<body>
<?php
foreach ($days as $day) {
    echo $day . "<br>";
} ?>
<?php foreach ($days as $day): ?>
    <?= $day ?> <br>
<?php endforeach; ?>
<table class="table table-bordered table-dark">
    <?php foreach ($days as $key => $day): ?>
        <tr>
            <td><?= $key ?> </td>
            <td><?= $day ?> </td>
        </tr>
    <?php endforeach; ?>
</table>


</body>
</html>