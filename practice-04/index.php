<?php
/**
 * Created by PhpStorm
 * User: 1
 * Date: 14.12.2017
 * Time: 10:31
 */
$user = "root";
$password = "";
$dsn = "mysql:dbname=geoworld;host=127.0.0.1;charset=utf8";
$connection = new PDO($dsn, $user, $password);
//Налаштовумо занесення даних з вибірки в асоціативний масив
$connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
//Формуємо SQL запит
$sql = "SELECT * FROM continents";
//Підготовка запиту до виконання(компіляція запиту)
//Зберігає запит від інєкій
$stmt = $connection->prepare($sql);
//Виконуємо запит
$stmt->execute();
//Отримуємо результат запиту і кидаємо його в масив
$continents = $stmt->fetchAll();
//Друк результату
//print_r($continents);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

</head>
<body>

<table class="table table-bordered table-dark">
    <?php foreach ($continents as $continent): ?>
    <tr>
        <td><a href="#">  <?= $continent [code] ?><a></td>
        <td> <?= $continent [name] ?></td>
        <td> <?= $continent [description] ?></td>
    </tr>
    <?php endforeach; ?>
</table>

</body>
</html>
